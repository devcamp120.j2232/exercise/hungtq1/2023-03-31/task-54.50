package com.devcamp.countryandregion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;

@SpringBootApplication
public class CountryAndRegionApplication {

	public static void main(String[] args) {
		SpringApplication.run(CountryAndRegionApplication.class, args);

		ArrayList<Country> countries = new ArrayList<>();
		Country vietnam = new Country("VN", "Viet Nam");
		vietnam.addRegion(new Region("HN", "Ha Noi"));
		vietnam.addRegion(new Region("HCM", "Ho Chi Minh"));
		countries.add(vietnam);
		countries.add(new Country("US", "Hoa Kỳ"));
		countries.add(new Country("JP", "Nhật Bản"));

		for (Country country : countries) {
			if (country.getCountryName().equals("Viet Nam")) {
				System.out.println("Regions of " + country.getCountryName() + ":");
				for (Region region : country.getRegions()) {
					System.out.println(region.getRegionName());
				}
			}
		}
	}

}
